FROM hashicorp/terraform:full

RUN apk add make git openssh-client


ENTRYPOINT ["/bin/bash"]
