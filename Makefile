.PHONY: build

IMAGE := registry.gitlab.com/jsantiagoh/terraform-ci

build:
	docker build -t $(IMAGE) .

run:
	docker run -ti $(IMAGE) 


push:
	docker push $(IMAGE):latest
